import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';

import { UsersCreds } from './../../models/usercreds.interface';

/*
  Generated class for the AuthProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/

/**
 * @author: SOUM SOM ON
 */
@Injectable()
export class AuthProvider {

  constructor(public aFireauth: AngularFireAuth) {
    // console.log('Hello AuthProvider Provider');
  }

  /**
   *  For logging in a particular user. Called from the login.ts file.
   * 
   * @param credentials 
   */

  public dologin(credentials: UsersCreds) {
    var promise = new Promise((resolve, reject) => {
      this.aFireauth.auth.signInWithEmailAndPassword(credentials.email, credentials.pwd)
        .then(() => {
          resolve(true);
        }).catch((err) => {
          reject(err);
        });
    });
    return promise;

  }
}
