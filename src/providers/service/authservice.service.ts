import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { LoadingController, Loading } from 'ionic-angular';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

@Injectable()
export class ServiceProvider {
  loading: Loading;
  constructor(
    public http: Http,
    public loadingCtrl: LoadingController) {
    console.log('Hello ServiceProvider Provider');
  }
  
/**
 * 
 * @param res 
 */
  public extractData(res: Response) {
    let body = res.json();
    return body || {};
  }

  /**
   * 
   * @param error 
   */
  public handleError(error: Response | any) {
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }

  /**
   * 
   */
  public showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    this.loading.present();
  }

  /**
   * 
   */
  public dismissLoading() {
    this.loading.dismiss();
  }

}
