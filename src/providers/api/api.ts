import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import { ServiceProvider } from './../service/authservice.service';

@Injectable()
export class ApiProvider {

  private apiUrl = 'https://restcountries.eu/rest/v2/all';

  constructor(
    public http: Http,
    private serviceProvider: ServiceProvider) { }
    
  getCountries(): Observable<string[]> {
    return this.http.get(this.apiUrl)
      .map(this.serviceProvider.extractData)
      .catch(this.serviceProvider.handleError);
  }

  // public extractData(res: Response) {
  //   let body = res.json();
  //   return body || {};
  // }

}
