import { Component } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import {
  IonicPage,
  NavController,
  NavParams,
  MenuController
} from 'ionic-angular';

// Interface of Users
export interface Users {
  email: string;
  password: string;
}

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  users = {} as Users;

  constructor(private afAuth: AngularFireAuth,
    public navCtrl: NavController,
    public navParams: NavParams,
    public menuCtrl: MenuController) {
  }

  /**
   * 
   * @param credentials 
   */
  doLogin(credentials: Users) {
    // try {
    //   const result = this.afAuth.auth.signInWithEmailAndPassword(credentials.email, credentials.password);
    //   if (result) {
        this.navCtrl.setRoot('TabsPage');
    //   }
    // }
    // catch (e) {
    //   console.error(e);
    // }
  }

  doRegister() {
    this.navCtrl.push('RegisterPage');
  }

  openResetPwd(){
    this.navCtrl.push('ResetpasswordsPage')
  }

  ionViewDidLoad() {
    this.menuCtrl.enable(false);
    console.log('ionViewDidLoad CountryPage');
  }
}
