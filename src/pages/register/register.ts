import { Component } from '@angular/core';
import { AngularFireAuth } from "angularfire2/auth";
import {
  IonicPage,
  NavController,
  NavParams
} from 'ionic-angular';

import { NewUser } from './../../models/newuser.interface';

// Interface of Users
export interface Users {
  userName?: string;
  email: string;
  password: string;

}

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {

  newusers = {} as NewUser;
  users = {} as Users

  constructor(private afAuth: AngularFireAuth,
    public navCtrl: NavController, public navParams: NavParams) {
  }

  /**
   * 
   * @param credentials 
   */
  async createNewUser(credentials: Users) {
    try {
      const result = await this.afAuth.auth.createUserWithEmailAndPassword(credentials.email, credentials.password);
      console.log(result);
      this.navCtrl.setRoot('TabsPage')
    }
    catch (e) {
      console.error(e);
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }

}
