import { Component } from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams,
  MenuController,
} from 'ionic-angular';

// import { ServiceProvider } from './../../providers/service/authservice.service';
import { ApiProvider } from './../../providers/api/api';

@IonicPage()
@Component({
  selector: 'page-country',
  templateUrl: 'country.html',
})
export class CountryPage {

  countries: string[];
  errorMessage: string;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public apiProvider: ApiProvider,
    public menuCtrl: MenuController) {

  }

  getCountries() {

    this.apiProvider.getCountries()
      .subscribe(
      countries => this.countries = countries,
      error => this.errorMessage = <any>error);

    console.log('countries', this.countries)
  }

  /**
   * openCountryDetails
   */
  public openCountryDetails(countries: any) {

    this.navCtrl.push('CountryDetailsPage', { countries: countries }).catch(() => {
      console.log("Empty data");
    })

  }

  /**
  * 
  * @param refresher 
  */
  public doRefresh(refresher) {
    console.log('Begin async operation', refresher);
    setTimeout(() => {
      console.log('Async operation has ended');
      // this.getCountries();
      // refresher.complete();
    }, 2000);
  }

  ionViewDidLoad() {
    this.menuCtrl.enable(true);
    this.getCountries();
  }

}
