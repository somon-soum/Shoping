import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ResetpasswordsPage } from './resetpasswords';

@NgModule({
  declarations: [
    ResetpasswordsPage,
  ],
  imports: [
    IonicPageModule.forChild(ResetpasswordsPage),
  ],
})
export class ResetpasswordsPageModule {}
